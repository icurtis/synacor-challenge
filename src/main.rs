#[macro_use]
extern crate serde_derive;
extern crate bincode;

pub mod vm;
pub mod instruction;
use std::fs::File;
use std::io::Read;
use vm::VM;

fn main() {
    println!("Entering the Synacor Challenge!");

    let mut f = File::open("../challenge.bin").unwrap();
    let mut program = Vec::new();
    let len_f = f.read_to_end(&mut program).unwrap();
    eprintln!("Length: {}\nVec: {}", len_f, program.len());

    let mut vm = VM::new();
    vm.load();
    vm.add_bytes(program);
    vm.run();

}
