#[derive(Debug, PartialEq)]

pub enum Opcode {
    HLT,
    SET,
    PUSH,
    POP,
    EQ,
    GT,
    JMP,
    JT,
    JF,
    ADD,
    MULT,
    MOD,
    AND,
    OR,
    NOT,
    RMEM,
    WMEM,
    CALL,
    RET,
    OUT,
    IN,
    NOOP,
    IGL
}

impl From<u16> for Opcode {
    fn from(v: u16) -> Self {
        match v {
            0 => Opcode::HLT,
            1 => Opcode::SET,
            2 => Opcode::PUSH,
            3 => Opcode::POP,
            4 => Opcode::EQ,
            5 => Opcode::GT,
            6 => Opcode::JMP,
            7 => Opcode::JT,
            8 => Opcode::JF,
            9 => Opcode::ADD,
            10 => Opcode::MULT,
            11 => Opcode::MOD,
            12 => Opcode::AND,
            13 => Opcode::OR,
            14 => Opcode::NOT,
            15 => Opcode::RMEM,
            16 => Opcode::WMEM,
            17 => Opcode::CALL,
            18 => Opcode::RET,
            19 => Opcode::OUT,
            20 => Opcode::IN,
            21 => Opcode::NOOP,
            _ => Opcode::IGL
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Instruction {
    opcode: Opcode
}

impl Instruction {
    pub fn new(opcode: Opcode) -> Instruction {
        Instruction {
            opcode: opcode
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_hlt() {
        let opcode = Opcode::HLT;
        assert_eq!(opcode, Opcode::HLT);
    }

    #[test]
    fn test_create_instruction() {
        let instruction = Instruction::new(Opcode::HLT);
        assert_eq!(instruction.opcode, Opcode::HLT);
    }
}
