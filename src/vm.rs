use instruction::Opcode;
use std::io::{stdout,stdin,BufRead,Write,BufWriter,BufReader};
use std::fs::File;
use std::collections::VecDeque;
use bincode::{serialize_into, deserialize_from};

/// Virtual machine struct to execute bytecode
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct VM {
    registers: [u16; 8],
    pc: usize,
    program: Vec<u16>,
    stack: Vec<u16>,
    verbose: bool,
    input_buffer: VecDeque<u8>,
}

impl VM {
    /// Creates and returns a new VM
    pub fn new() -> VM {
        VM {
            registers: [0; 8],
            program: vec![],
            stack: vec![],
            pc: 0,
            verbose: false,
            input_buffer: VecDeque::new(),
        }
    }

    /// Adds an arbitrary byte to the VM's program
    pub fn add_bytes(&mut self, b: Vec<u8>) {
        let mut i = 0;
        while i < b.len() {
            let result = ((b[i + 1] as u16) << 8) | b[i] as u16;
            self.program.push(result);
            i += 2;
        }
        eprintln!("LEN: {}", self.program.len());
    }

    fn decode_opcode(&mut self) -> Opcode {
        let opcode = Opcode::from(self.program[self.pc]);
        self.pc += 1;
        if self.verbose {
            eprint!("{:06} ", self.pc);
        }
        return opcode;
    }

    fn save(&mut self) {
        let mut file = BufWriter::new(File::create("save.bin").unwrap());
        serialize_into(&mut file, &self).unwrap();
    }

    pub fn load(&mut self) {
        let file = File::open("save.bin");
        match file {
            Ok(f) => {
                println!("Loading!");
                let mut bf = BufReader::new(f);
                let decoded: VM = deserialize_from(&mut bf).unwrap();
                self.registers = decoded.registers;
                self.program = decoded.program;
                self.stack = decoded.stack;
                self.pc = decoded.pc;
                println!("Done! Hope you remember what the last command was.");
            }
            Err(_) => println!("No file. Not loading."),
        }
    }

    fn next_16_bits(&mut self, get_reg:bool) -> u16 {
        let result = self.program[self.pc];
        self.pc += 1;
        if result > 32775 {
            return 0;
        } else if result>=32768 {
            let result_reg = result - 32768;
            if get_reg == true {
                return result_reg;
            } else {
                return self.registers[result_reg as usize];
            }
        }

        return result;
    }

    pub fn run(&mut self) {
        let mut is_done = false;
        while !is_done {
            is_done = self.execute_instruction();
        }
    }

    pub fn run_once(&mut self) {
        self.execute_instruction();
    }

    pub fn show_vm_state(&mut self) {
        println!("");
        println!("REGS:       [{}] [{}] [{}] [{}] [{}] [{}] [{}] [{}]",
                 self.registers[0], self.registers[1], self.registers[2], self.registers[3],
                 self.registers[4], self.registers[5], self.registers[6], self.registers[7]);
        println!("STACK:      {:?}", self.stack);
        println!("PC:         {}", self.pc);
    }

    fn execute_instruction(&mut self) -> bool {
        if self.pc >= self.program.len() {
            return true;
        }

        if self.pc == 6027 {
             println!("Ackermann function called.");
             // self.registers[0] = 6;
             // self.registers[7] = 25734;
             // self.pc = self.stack.pop().unwrap() as usize;
             self.show_vm_state();
        }
        match self.decode_opcode() {
            Opcode::HLT => {
                println!("HLT encountered");
                return true;
            }
            Opcode::SET => {
                let register = self.next_16_bits(true) as usize;
                let number = u16::from(self.next_16_bits(false));
                self.registers[register] = number;
                if self.verbose {
                    eprint!("SET: {} {} reg:   ", self.program[self.pc-2], number);
                    for n in self.registers.iter() {
                        eprint!("{} ", n);
                    }
                    eprint!("\n");
                }
            }
            Opcode::PUSH => {
                let a = self.next_16_bits(false);
                self.stack.push(a);
                if self.verbose {
                    eprintln!("PUSH {}", a);
                }
            }
            Opcode::POP => {
                let register = self.next_16_bits(true) as usize;
                let a = self.stack.pop().unwrap();
                self.registers[register] = a;
                if self.verbose { eprintln!("POP {}", register); }
            }
            Opcode::EQ => {
                let register = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                if self.verbose {
                    eprintln!("EQ {} {} {}", self.program[self.pc-3], b, c);
                }
                if b == c {
                    self.registers[register] = 1;
                } else {
                    self.registers[register] = 0;
                }
            }
            Opcode::GT => {
                let register = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                if self.verbose {
                    eprintln!("EQ {} {} {}", self.program[self.pc-3], b, c);
                }
                if b > c {
                    self.registers[register] = 1;
                } else {
                    self.registers[register] = 0;
                }
            }
            Opcode::JMP => {
                let newpc = self.next_16_bits(false) as usize;
                if self.verbose {
                    eprintln!("JMP {}", newpc);
                }
                self.pc = newpc;
            }
            Opcode::JT => {
                let a = self.next_16_bits(false) as usize;
                let newpc = self.next_16_bits(false) as usize;
                if self.verbose {
                    eprintln!("JT {} {}", self.program[self.pc-2], newpc);
                }
                if a != 0 {
                    self.pc = newpc;
                } else {
                }
            }
            Opcode::JF => {
                let a = self.next_16_bits(false) as usize;
                let newpc = self.next_16_bits(false) as usize;
                if self.verbose {
                    eprintln!("JF {} {}", self.program[self.pc-2], newpc);
                }
                if a == 0 {
                    self.pc = newpc;
                } else {
                }
            }
            Opcode::ADD => {
                let reg = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                self.registers[reg] = (b + c) % 32768;
                if self.verbose {
                    eprintln!("ADD {} {} {}", self.program[self.pc-3], b, c);
                }
            }
            Opcode::MULT => {
                let reg = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                if self.verbose {
                    eprintln!("MULT {} {} {}", self.program[self.pc-3], b, c);
                }
                self.registers[reg] = ((b as u32 * c as u32) % 32768) as u16;
            }
            Opcode::MOD => {
                let reg = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                self.registers[reg] = (b % c) % 32768;
                if self.verbose {
                    eprintln!("MOD {} {} {}", self.program[self.pc-3], b, c);
                }
            }
            Opcode::AND => {
                let register = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                if self.verbose {
                    eprintln!("AND {} {} {}", self.program[self.pc-3], b, c);
                }
                self.registers[register] = (b & c) % 32768;
            }
            Opcode::OR => {
                let register = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                let c = self.next_16_bits(false);
                self.registers[register] = (b | c) % 32768;
                if self.verbose {
                    eprintln!("OR {} {} {}", self.program[self.pc-3], b, c);
                }
            }
            Opcode::NOT => {
                let register = self.next_16_bits(true) as usize;
                let b = self.next_16_bits(false);
                self.registers[register] = (!b) % 32768;
                if self.verbose {
                eprintln!("NOT {} {}", self.program[self.pc-2], b);
                }
            }
            Opcode::RMEM => {
                let reg = self.next_16_bits(true) as usize;
                let mem_b = self.next_16_bits(false) as usize;
                if self.verbose {
                    eprintln!("RMEM {} {}", self.program[self.pc-2], self.program[self.pc-1]);
                }
                self.registers[reg] = self.program[mem_b];
            }
            Opcode::WMEM => {
                let mem_a = self.next_16_bits(false) as usize;
                let b = self.next_16_bits(false);
                if self.verbose {
                    eprintln!("WMEM {} {}", self.program[self.pc-2], self.program[self.pc-1]);
                }
                self.program[mem_a] = b;
            }
            Opcode::CALL => {
                let jmpto = self.next_16_bits(false) as usize;
                if self.verbose {
                    eprintln!("CALL {}", jmpto);
                }
                self.stack.push(self.pc as u16);
                self.pc = jmpto;
            }
            Opcode::RET => {
                if self.verbose {
                    eprintln!("RET");
                }
                let jmpto = self.stack.pop().unwrap();
                self.pc = jmpto as usize;
            }
            Opcode::OUT => {
                let printchar = self.next_16_bits(false) as u8 as char;
                print!("{}", printchar);
                if self.verbose {
                    eprint!("OUT {}", printchar);
                }
            }
            Opcode::IN => {
                let reg = self.next_16_bits(true) as usize;
                if self.verbose {
                    eprint!("IN {}", self.program[self.pc-1]);
                }

                while self.input_buffer.is_empty() {
                    print!(">>");
                    stdout().flush().expect("Could not flush stdout");
                    let mut line = String::new();
                    let lstdin = stdin();
                    lstdin.lock().read_line(&mut line) .unwrap();
                    line = line.to_ascii_lowercase();
                    if line.eq("save\n") { // char: \
                        self.save();
                        println!("GAME SAVED!");
                    } else if line.eq("fix_teleporter\n") { // char: !
                        self.program[6027] = 1;
                        self.program[6028] = 32768;
                        self.program[6029] = 6;
                        self.program[6030] = 18;
                        self.registers[7] = 25734;
                        println!("TELEPORTOR CALIBRATED");
                    }

                    for c in line.into_bytes() {
                        self.input_buffer.push_back(c);
                    }
                }
                self.registers[reg] = self.input_buffer.pop_front().unwrap() as u16;
            }
            Opcode::NOOP => {
                if self.verbose {
                    eprintln!("NOOP");
                }
            }
            Opcode::IGL => {
                println!("Illegal instruction encountered: {}", self.program[self.pc-1]);
                return true;
            }

        };
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_vm() -> VM {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 5;
        test_vm.registers[1] = 10;
        test_vm
    }

    #[test]
    fn test_create_vm() {
        let test_vm = VM::new();
        assert_eq!(test_vm.registers[0], 0)
    }

    #[test]
    fn test_opcode_hlt() {
        let mut test_vm = VM::new();
        let test_bytes = vec![0,0,0,0,0,0];
        test_vm.program = test_bytes;
        test_vm.run();
        assert_eq!(test_vm.pc, 1);
    }

    #[test]
    fn test_opcode_igl() {
        let mut test_vm = VM::new();
        let test_bytes = vec![200,0,0,0,0,0];
        test_vm.program = test_bytes;
        test_vm.run();
        assert_eq!(test_vm.pc, 1);
    }

    #[test]
    fn test_set_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![1, 0, 500, Opcode::SET as u16, 1, 1];
        test_vm.run();
        assert_eq!(test_vm.registers[0], 500);
        assert_eq!(test_vm.registers[1], 1);
    }

    #[test]
    fn test_eq_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![4, 0, 500, 500,
                               4, 1, 200, 32767];
        test_vm.run_once();
        assert_eq!(test_vm.registers[0], 1);
        test_vm.run_once();
        assert_eq!(test_vm.registers[1], 0);
    }

    #[test]
    fn test_gt_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![5, 0, 500, 500,
                               5, 1, 32767, 2];
        test_vm.run_once();
        assert_eq!(test_vm.registers[0], 0);
        test_vm.run_once();
        assert_eq!(test_vm.registers[1], 1);
    }

    #[test]
    fn test_add_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![9, 0, 500, 12,
                               9, 1, 200, 32767];
        test_vm.run_once();
        assert_eq!(test_vm.registers[0], 512);
        test_vm.run_once();
        assert_eq!(test_vm.registers[1], 199);
    }

    #[test]
    fn test_and_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![12, 0, 500, 12,
                               12, 32769, 200, 32767];
        test_vm.run_once();
        assert_eq!(test_vm.registers[0], 4);
        test_vm.run_once();
        assert_eq!(test_vm.registers[1], 200);
    }

    #[test]
    fn test_or_opcode() {
        let mut test_vm = get_test_vm();
        test_vm.program = vec![13, 0, 500, 12,
                               13, 32769, 200, 32767];
        test_vm.run_once();
        assert_eq!(test_vm.registers[0], 508);
        test_vm.run_once();
        assert_eq!(test_vm.registers[1], 32767);
    }
}
